import React, { Component } from 'react'
import ContactForm from './../components/ContactForm'
import {PageContainer} from './../styles/ContactForm'

export default class SmartContactForm extends Component {
    render() {
        return (
            <PageContainer>
                <ContactForm />
            </PageContainer>
        )
    }
}
