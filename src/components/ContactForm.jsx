import React, { Component } from 'react'
import {
  FormElement,
  FormContainer,
  FormFooter,
  GlobalStyles,
  FormTitle,
  FormCheckIcon,
  FormContent,
  FormArea,
  FormRadioArea,
  FormRadioItem,
  FormInput,
  FormRequiredItem,
  FormInfo,
  FormInfoTitle,
  FormInfoImage,
  FormInfoList,
  FormInfoListItem,  
  FormContentTitle,
  FormContentSubtitle,
  Submit
} from '../styles/ContactForm'

const emailRegex = RegExp(
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
)

const phoneValidator = props => {
  if (props.length < 5) {
    return -1
  }
  let phoneNumber
  let phoneMinNumbers = 10 // Minimal Phone Numbers
  let phoneMaxNumbers = 12 // Maximum Phone Numbers
  let numberPattern = /\d+/g // Regex to clean string and keep just numbers
  let limitSequenceNumbers = 3 // Limit to consider sequency numbers
  let limitRepeatNumbers = 3 // Limit to consider repeat numbers
  let lastNumber // Last Number Checked
  let actualSequence = 1 // Actual Sequence of Numbers
  let actualRepeat = 1 // Actual Repeat of Numbers
  let aggregateSequence = 0 // Sequence Total
  let aggregateRepeat = 0 // Repeat Total
  let score = 0 // Final Score
  // Check Number Length

  // Only Numbers
  phoneNumber = props.match(numberPattern)
  
  if(phoneNumber != null){
    phoneNumber = phoneNumber.join('')
  }else{
    return -1;
  }
  if (
    phoneNumber.length < phoneMinNumbers ||
    phoneNumber.length > phoneMaxNumbers
  ) {
    return -1
  }
  // Convert to Array
  let phoneNumbersArray = phoneNumber.split('')
  // Check Sequence and Repeat
  phoneNumbersArray.forEach((item, index) => {
    let actualNumber = parseInt(item)
    if (index > 0) {
      if (checkRepeat(lastNumber, actualNumber)) {
        actualRepeat++
      } else {
        aggregateRepeat =
          actualRepeat >= limitRepeatNumbers
            ? aggregateRepeat + actualRepeat
            : aggregateRepeat
        actualRepeat = 1
      }
      if (checkSequence(lastNumber, actualNumber)) {
        actualSequence++
      } else {
        aggregateSequence =
          actualSequence >= limitSequenceNumbers
            ? aggregateSequence + actualSequence
            : aggregateSequence
        actualSequence = 1
      }
    }
    lastNumber = actualNumber
  })
  // CHeck Last Item
  aggregateRepeat =
    actualRepeat >= limitRepeatNumbers
      ? aggregateRepeat + actualRepeat
      : aggregateRepeat
  aggregateSequence =
    actualSequence >= limitSequenceNumbers
      ? aggregateSequence + actualSequence
      : aggregateSequence

  function checkRepeat (last, actual) {
    return last === actual ? true : false
  }
  function checkSequence (last, actual) {
    return last === actual - 1 || last === actual + 1 ? true : false
  }
  // Calculate Score
  score = 100 - (aggregateSequence + aggregateRepeat) * 12
  return score
}

export default class ContactForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
      gender: 'male',
      firstName: null,
      lastName: null,
      email: null,
      phone: null,
      phoneScore: null,
      errors: {
        gender: '',
        firstName: '',
        lastName: '',
        email: '',
        phone: ''
      }
    }
  }

  formValid = errors => {
    let isValid =
      this.state.firstName === null ||
      this.state.lastName === null ||
      this.state.email === null ||
      this.state.phone === null
        ? false
        : true
    Object.values(errors).forEach(value => {
      value.length > 0 && (isValid = false)
    })

    return isValid
  }

  handleChange = event => {
    let { name, value } = event.target
    this.setState({[name]:value});
    let errors = this.state.errors
    switch (name) {
      case 'firstName':
        errors.firstName =
          value.length < 3 && value.length > 0
            ? 'Minimale 3 Zeichen erforderlich'
            : ''
        break
      case 'lastName':
        errors.lastName =
          value.length < 3 && value.length > 0
            ? 'Minimale 3 Zeichen erforderlich'
            : ''
        break
      case 'email':
        errors.email = emailRegex.test(value) ? '' : 'Email ist ungültig'
        break
      case 'phone':
        errors.phone =
          phoneValidator(value) >= 0 && value.length > 0
            ? ''
            : 'Telefon ist ungültig'
        this.setState({ phoneScore: phoneValidator(value) })
        break
      default:
        break
    }
    this.setState({ errors, [name]: value })
  }

  handleSubmit = event => {
    event.preventDefault()

    if (this.formValid(this.state.errors)) {
      alert(`
        ----Success---
        Gender: ${this.state.gender}
        First Name: ${this.state.firstName}
        Last Name: ${this.state.lastName}
        E-mail: ${this.state.email}
        Phone: ${this.state.phone}
        Phone Score: ${this.state.phoneScore}`)
    } else {
      alert(`----Error---
        Gender: ${this.state.gender}
        First Name: ${this.state.firstName}
        Last Name: ${this.state.lastName}
        E-mail: ${this.state.email}
        Phone: ${this.state.phone}
        Phone Score: ${this.state.phoneScore}`)
    }
  }

  render () {
    return (
      <div>
        <GlobalStyles />
        <form onSubmit={this.handleSubmit}>
          <FormElement>
            <FormTitle>Fast fertig!</FormTitle>
            <FormContainer>
              <FormCheckIcon />
              <FormContentTitle>
                An wen dürfen wir das Angebot versenden?
              </FormContentTitle>
              <FormContentSubtitle>
                Im nächsten Schritt sehen Sie Ihr Angebot im Detail und können
                dieses flexibel anpassen.
              </FormContentSubtitle>
              <FormContent>
                <FormArea>
                <FormRadioArea>
                  <FormRadioItem
                      value='male'
                      name='gender'
                      checked={this.state.gender === 'male'}
                      onChange={this.handleChange}>
                    Herr
                  </FormRadioItem>
                  <FormRadioItem
                      value='female'
                      name='gender'
                      checked={this.state.gender === 'female'}
                      onChange={this.handleChange}>
                    Frau
                  </FormRadioItem>
                  
                  </FormRadioArea>
                  <FormInput
                    type='text'
                    placeholder='Vorname *'
                    name='firstName'
                    error={this.state.errors.firstName}
                    onChange={this.handleChange}
                  />
                  <FormInput
                    type='text'
                    placeholder='Nachname *'
                    name='lastName'
                    error={this.state.errors.lastName}
                    onChange={this.handleChange}
                  />
                  <FormInput
                    type='text'
                    placeholder='E-mail *'
                    name='email'
                    error={this.state.errors.email}
                    onChange={this.handleChange}
                  />
                  <FormInput
                    type='text'
                    placeholder='Telefon *'
                    name='phone'
                    error={this.state.errors.phone}
                    onChange={this.handleChange}
                  />
                  <FormRequiredItem>* Pflichtfeld</FormRequiredItem>
                </FormArea>
                <FormInfo>
                  <FormInfoTitle>Ihr Angebot von Benu</FormInfoTitle>
                  <FormInfoImage src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMYAAACSCAIAAACR5iLrAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyVpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ4IDc5LjE2NDAzNiwgMjAxOS8wOC8xMy0wMTowNjo1NyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIDIxLjAgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QkZDQ0ZENzk0REM1MTFFQTk4QkREQTVEMzYyOTM0RTUiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QkZDQ0ZEN0E0REM1MTFFQTk4QkREQTVEMzYyOTM0RTUiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpCRkNDRkQ3NzREQzUxMUVBOThCRERBNUQzNjI5MzRFNSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpCRkNDRkQ3ODREQzUxMUVBOThCRERBNUQzNjI5MzRFNSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pu7sJSgAADn3SURBVHja7H0HlFzndd7r/b3pZWcLdhdYFDaAYlOhRFKSRUqMaMqKJDsqlpPYkuV27JMcx0mcyHEsn8TxiXMSR7ZPnFihbMeSEsoqlGirkGIRQbEDBEgsgMVid2en19dr7v/e7GB2sbtEIzEQ5qcEDGbevHnvf99/73fvfwt+6tQp0zSx0RiNSzFYlqUATwzDjOZiNC7JsG2buvizNJtN3/eH6sYkSYLlMnrAl2UMQMr38WoVw7Egk8VwHN7ArTLumz47jhHosLbbrnutcTrPEmeelud5mUwmlUoN1V0dO3ZsBKnLDyny6MtTX/kivDj1c78UTE1hZmls5a0MhRWZf2OPfVLz9I+UPukKwbia+8LUn/a/VavVbrjhhmG7q8nJyVKpJAjC6AG/8YM4A65jr/beOvIS+qv9PE2ivxn7FfizYlcd1kVv8+rg96enp4fwrnieB/E5erqXGVLWPfdWWbEixLx334P+nb1n1f9kTd2j5n8dQYeb+kXin5Bt/3eFf9n/SrFYBNYynDcGunhkyV6Wgb/yyisXbPHRNJ1MJof23o4fPy7L8ugZv5HDcRzigr9cqVRisdgw3148HgebdvSY32h6Xq1WKepCXAnZbJYkyWG+N9B9hw8fDoJg9JjfsAEUFp+fny8UCqO5GI1LMsrlMnWpztVut0euoKtkqKqaTqe3VHyD//B9nyCITaXZBh0H2mRlZSWXy8H70VdAew4zVR+NSwupc3IiAFf/xje+senWSqfTcV13EGFf+tKXdF3/8z//89OnTw8eWa/XoxenFxcRRjdzDpmGAdQeJOT213306NHXvDdd00YP+LIPgMfmUurrX/96IpF48skn4fXtt9/ee/ymCWACCVSr1fL5fB98H/rQh0A4NRoNsKoGT9dsNF584QWQXg8//PAHfuqnTpw4AZgFesxxHEB7z5490zMzcKrC+Pizzzxz8KmnyqXSDfv3pzMZTVVByAHO4PpmZmbgsCcee6xaqeTHxk4tLIBWzWQy+w8cgJ+eP3bsbbffXqtW/+7hh2+57bZ6rfbmt7xl9Fwv49igwc5Iqbvvvhsg8ra3va2PJyyEQuTMHPQXwCkeeeQRgAiovw1SjReEd77rXSvF4o6pKUDGgf3753bvhvevve46kCgASvhKKxw33XwzIPjmW24BoDx98OCxY8fgtxYXF1vN5o7paTgY/gRogjy79bbb4Ax33nUXDiK323Vc19D1YrE4MzsrS9KevXtHD/Xyjg1+zXUWXySQzv4OyAaapjewrmeeeeb666/neT56R9M0ECSb0PZWKzYgyUBEAXSKKyu79+wZPYwrdADb2Yqeb7T4tnJQbcATEm4Eceutt67DZhi8cPaIrdeM0aWM8HRFj62e9SYW38WMiC2NpvtqGMBYtoPU/MKp5WptNE2jcUmGqWlUIpdXhiyAbjSu3NFtNojRLIzGpR0jSI3GCFKjMYLUaIwgNRqjMYLUaAzNoIbhIlDg5VUYe4mj0Qs6vSS3H2Vfbuva/vGHVDShvu8vt9pXF6hwvKBIFEnCFCy3O54fXDwQcrLIhltnlxdVl19KAao8z2ubFlqp4TKDdy77UnsDRlbkSYIIAr9jmghScMuAKzzYdNX1J2QwlB4PA4f6HyU5hqGoq11KRSNK41x46bn83HW4bx164om9t91OErhtauXTi/mpHYFjampXzo7TNG1pqhCPa602zbIESQAMHUNrtzo7wiiaK2jASsJo2vd8LJTUK8dPKKkUzfKWobertUxhDCdJy7SMVp0RJIyiOYaiWa5dLWMUrzcbYkymaLrTbORn5mgGPUfAZU+HXuVSKppe+L+cjJcXT2bGclq7XZw/4rq+2qjyAv/q009O7LvOd712eWX55NL49HRtebleWhVkkRPlRr05sfuaVrly5UFq7W94/jiBdWq1br2KkWynVo2l0688fSqR30GQfmNlkY9l240a7tuZwpQYl088/2y2MNkqL+uG6xpqPD9JM1JfgAWh9LqcKv2HLx66jHt8QTgsy5xvtM+I9P4620wRoLd9z9ItThoseQCK4wqzXmfjCs9xnuvM15ruxRHJSNlNSGJMEuH1pgkEb8zoNhtDIqUwhePOrNtzGaL4Y2DvRUJF5jgfuwS2CUkMxaIaDkgF2Huu3UMQV5GTDIzcI6eXI2TduXcXc1aQ4wXM4dGlZX8EqQHjBSeuAitvUE/jA/IqunfbsoKwNmHgo6B+gsT7wmyjQeO6Vpib73tAygOSpASBH5Lpo4Ztrk1dA1IFtgwwAl03FEVxXafdUZOJeDS5UVKh57kw3UOeQX/uw3GcU9Wa6HnVRt2xPY4HSzAgCNKwrLgi4gTZadYBXZwg79m7BxTcC88eFGKZRqNeWlkyLDs3vuPdd96OjSC1qTfB9RxYgq5rV1ZXXnhlwQf6auqNRmvXrp3trkZRlO16LInpuk5SVCY9Zph6gHumbtz/gQ8y9JWKMJqm5wpjruMkM+neOgkXUOiiwwBbnXZKlCT4KFpXN97yFhBjQTDjvelGIAxD5cYbLkjBlJ08+tzpE0fe8q4PJDO5fL07MTW1ePL4vr37Ysl0o9F0HFuWRbXdjiWSMI9qp1WYLLAU9sgjj/wYqM1yqQQgYkWp1ayzNGM73szsDAAGPoqvD/cmwoENZJoMTzWR4XAimOaB2RnTBDFvbfAIn/t5wh0zHBTlleKUOrK4hIUafN/EeLSR4rqeZZksx4G4IgnS9UADcvg5z8DR08teOAkjJ8KZSbnIaogE2t8Y9s2cba6w22rB/XfbLYykeY5x0LBUVU8kE3q3Q1BULJaIhNYwjyGClBSOq8Ti8zarPQFIM03LAVFNOJahewEWeG2G48qloiQqiwsnrr0BmBM1gtS5u2q8UNDgIP9JkoicN1vZdPBRZHrDt8IXeGQ3RdmtwyaofLT7Fgww7s0vL55Kxbc+STqTviJWyxBB6rsPf7Nc65i2zXEcy1DFYqUwMa6p3VQm5zkmmDrtZr3ZNbOZ1DV7Zl8+etwNiFvedN3Tz74QuG6r1frYJ3/uBz94zDW15dUKYlS+A+cRgeq2WjGJ5wWxWq37JPWee96XT72h9SBXFuZPVTqeWlGSuYNPHSRY6Ybrr735pgObepsAfCRFRBD0PFg2eLhIIqK47mDXsVXdcB2X51jL8RJxZQSpjePtd77bBX0QiR8swEOjxrEdFG6A4zRJrqysZLNZsHFIgpid2wfzzDLMxNQ0geF+4AmC+M47budY1rLtiJ9G3B8scHgY8LQomq6Vy0nljd7JKUzvVDI6Q19r286uud0IJWf5yl3XLTXblO8068gvRQsCGfgsx3SBQjEcZmhBfPzaHdnBr7xy+CUjYMorp3zXwWjh/ffeM7L4Nlp8kbK7erjU2RZfZKBEfiYw/QKMYBk6DCDzHNdnKBIfcEFFUxfFHYQrhxhZfBuHquvW1VEAGIQrj7bJN47K6kqj1WVYWjftyUK22eqobVWOC5ruelabFxOm4+7cOSuE392gC4fHLzVEkKJI0qfoqwFS1BY2RzqbT6SzIGQCP6AoUpLjbt4N8IAE3Y+jKJ9oq2pEz891AA3irpoCsps6EUjA0Xo324XVD7+8Y5R0NRo/vlKq73CyHIckSdeywJSGP6WYbBimbVssy8P7ofsK9z2XYhjMB97q0bCUcTwktoibBlgANiBG4ChABGxykmy324os+8BhScIxLU4QXdcG7sHx/JCErYVE+yJCfIdpz2DoIOW4zqlqfc/4WKPd0kw7sKxqrUhQCst4p5eLosBjKJmEYDmq3eqMjeVU3TA1zbCsbDqja91GRyvkM/BePJGwNK3Wak1MTK7CKK5wHG+auiwqrl9SEvHK6ur03N6YyA/DXduOc2q5eMGI4hh6amj6IQwdpFiG3TeBZidfGN/w0cQkrEaQNVjPSJ4KKS0KiXFJcvMbCQ/BspsVEQUIDk8cKdiAe2anL1LOjSB1IQPfLPZzKzxtPy4ofO+yp5+MuNSF0inEolyHCL2fEX/q48Z1XIqmXNcF8kFT9NrqDFCk8WZnMw0dJykQA31hBqKuH4DruC5FED6SfDickyBI+LRvZ3nwzlrUG/yQoekkTbEsC6eyHbQRuSFmPBKicLxjmxTFoC+iJYBv1fbijOKzjFOLy7t27YRzAHHkBWH7sOnllRWWpkVJbrWaMFk0w2YzqRGkNh+mZR5ZLu2fmVo+dbLeascTaVtXBQFXbYrBPD6WqCyt8JKQSqdLxSInyphnweybHkPRuG3ZEokQkUlmStUyQIUmBTmhlEsloPcJRSrXqtm4gnOxpMSeKq7EBDGgWL3bsmw3lUiaho0RLs/LK6urOwrZSq0p8axmOhTPgtGfTMQAGvV6U7eseDJD4a5P4BzDaY0KRki63eSlVCYZa7YaFMVRvq6bOCeztWqbwj1OkuOSXGs0KZ7fMT4u8Bv9nDTDVUrFF5//ESyL06X2hz/8wfF8dqsp6jZrP3r2hWt2Tjx58OlmvS6JvJzIvudddwyLJhm2DZn+JoOmqa7rReEuKNEbC+UWQYXhCTgcZ5gWz/Ng1mHhFkQQiggqPDIqX3FGSyFrKujXtICjox/uix9s7SuRSxoJFbyXQLh2GLIJzpQDQa/9/sHwpuN5ILHOlM1AKYhEmIvfOz9cYT96Ag47e0PGQ8MPJSiqFr6NXoYT2rYNx0ThZX5oLLIsM9qQ2Yot9bJC65Wy42PzC4u5pBxTUtXVFY9mYrJUWi1ygpBJZyrlEihBWVE0TUOKi6SUmDI+liPOpYDJtp9GjxPf7NoGjxo8mFzTleuPWfev7dkbGY5znKKoqdjg8SN6/trzNrFjGmZpJlpxOC7KYvQynU5H4iEWi/XlBD5YWOdKHKEcHdHz13dssOP6lLkv1c9+ceUO23VKF1F9nqGpXDo9gtR2Fl9YL6LPfhD58VGlIBSVFnIIP3SP99hPFNoRNUjqa5q+6IpeBGAzIvMQsZnI/hqqyE/gYVOFsZFf6nUZhmUtVmoz+VxxZcl3PTDyPScgcY8VRduxSZzsaFohn6uWy5wouo5H4YGLBYogaMCQTUNWZLDvpFiKZ6lWq8VyrO34HMc4luZ7OMtzaqetJDPTU5PDllYahO4HtDzOClkZiJHCBhOVhzN3Y+ggxTPM3PgYmHg7pnao3Y5tW/FEOrLMfM8nkI8KBaSn0ymQN9G+Xr1WjyeTeYoslcrZTCaXywdhIvJYoYCkFEimXlkXJLSiZOVh05We61YqJYrmbMsMvVmEwHOGYQq81GrXBVE2dZ3nOdN0KJpwbJvjBbBaLccVBc71glQqOYLUdvYMGSIInroSi69nV2fspggT0evcWu/J8fX7XJuGiQ5nzjvccSKZJtBWtwSSiMDRf4IgBr6f4+DuAkWS4B1ZjnaXsbBIUiD1vBvkSEqdg7MqLCCBh5RnUPgHfoANlKfoV03dYO5FnwxkmQb9KoTYWrVZfC0+fTBvpUfOeh6EtfCA8IPwJ7BexaIzX+9pon7RUYBspMKwAb9XiBJ8GyXlu06lXOlouiyIWJixDjLZ8VxL1wRJAmARFG1bNqDHNA2BYxiWd10gAg7MB89xU9NTI0htw6XMU5X6VDZTWV12nABoEEfhjVYTx6nC5A6j1S1VSzDlDEPrhgms1nJsmqSTiXilXgs8LJtNl0srsiRohoeyK30X90EyBXAqmsbGxsabLb3RbkmMR3FKp92lONZzvamxTK3RsWyD5kRb13yg8K6tKHGj03FwnGc4nPRZRrAci/RxL3BtwzYdZ9fu2VK57rlgrqHdocLYWKPZAA3F8DwR1negQSLCgydp0E0Hrr9uG9pD0GwGdHaAGYZBM2wYfEd6YKZ4yHnZUdW4goJzAEAUScDPkRTlOA4b5m5wLDeSUttyKZbdXcjDWs9l8xRFkxQJ0yrIMZYJnXtJ5ZpcOlr5/dRkmqJhurO5rOO4LMtk0imY8ai/N6rx4vvwGHz0JwqrkpXEmJulKQoMRK8Q9gDHkcEVT2QcF2X34kQB7eLZFsUyBNqtsxFpC41Q0Ev1ZicRU/ywWiuQG1GUQb3CMQzLggiNh+16oy1IHO0OgjBDWsmyne1pNMghMGMBkclYwgPy5weAUgJHKe0AK6bnWwfgAp4CZL3SNFwbkEsgYR7tjyD1GrwiojuCsFY3kSCktd3ZwU3awV6mEZuImuxGHix66xjtgejbdccAfDmOjZQeTfV+neX4QZVcyAuD4OBYJjyY6p1gLXZ+w6+fQ7yv36w3A8xfLZVAAvM0He66oCBEWCRqV2UFDguIwPXxMPU1SVDNdtN3/anpHRQ54lKXlHdd6miT19jGeZ1MdoKks/lM5EGPLA9UaaOXOU1k0mng5lGMQxA61UDo5hFt74U5jIoBbTdM2y42W/lEYnVpkaQ4ArMxPwDuZLgW0NZ0KmmZOrBSF3iT49AMZdvA4vF2F9hGzHOdVCa9VFxlCCKRSLaadSKMMKFBN/kBTWCgOYD3AmyS2bwsCMNz165tra6sArIYimA5znGRc4TASRBUwBrDoBms22lSSH7RFMqXwdWuRjKkwImWacaSSVHgRpDawo9MUWlFhpnLZrIB6EDCM01T5MVKvZlIxEG1wTKVFc71XDwM70QsJAiSyRQsYjTbFDmeHyMJDKXbMEDFaNd1YAlTDAOMBznQfdRlnhyy1CUw6ERZgrsD6g1/EoSDipgHAc+zQNuAk8E6yI9PDUbFpAdCVSOfwghSW0wuQSg8oi/yWqUoUUQvJnkhEu9s8jXcen0LiNuQfjlMYulsxwlBUmGwYRBW5AjCfSeXohlkCmAB0HAyZPpb6rih2XSmhniS1+02nBddOMedinM77Hzp2oVskwQeyFBT7XZBHIHk9TzkEaNp0jTtILT44JhatcyAogcVjkw/NCPxREKR5WF7cMOYIVPvqDFJqldWo4C70KQiGY4zdYukCIHnG802PGVJEkFNOLZJ0izmOyRJG6YFR7IsbVoOSRCu5wHzgGfl+chMJ0gCTDNRUTqtJqq94fqaixwEPAeSAIczoG0cFHZFwXlIHKfAmnMdw7JZhjEdl2doTTMYhkTKFTRrgNnAzyhUCISkKQ9ssTCBC0RjV+2CxsUJzLAceAHXA8fgBKkoiihuLilxio7HE/gaJJEyDy8mlF4k4AskVpymbdsB3Q+fwk0h5xxNAysInatI6Y8gtYXiQ/4ckmNo+I+kOV3rEKiViuH6ActwareFNuhIFGYOB7QaDThK5oTiyoIkyq4XxOMKMohCOcEJgqGqwKJwxyEIFuAnCZyhaRwnhPULWcf0aY4nMZTs51gurHuJ51TTgu82Gs3s2BjNsPBsgdUAjj3XjsXjqtqlSJ9m+HarQ3NMp91mGI4XaN0xaIqReMHHfMCxaVjZdMJ2VYCsoZuCKJLAiLYtYFcplzhWMHSVYTmYAT2srExgBMtzOKwWXUulks1mKxaP6WqX5QXMs1qNeoBTFFgb6exISm05YDYzMSV0CNGO4zEECQ9SMwwasENSjsO7LggM0g0AJ1Y6nTEtA6fwVGbMMgxF4X3HbBu2yCNh0G42AT0URYgCmEU6RTGG7RKh/ECshcB5jhUlUVNV23HgVziAF0lEdfgmJyY2vbx0usfkEusp3WCstSz1lJEon0chq3QqAyInnkg6rg03SzFUgHa4aRDPcGt8Mu57bkyRCDzgBQFtg+NUJj/Gha44bBTcci7DA6liBGCvrawsg/GME6YSi4F1bVtmPKZ0u6BcgMziokCXVlczqUTTAB1lZzNxHHM6nTYviGAu6bolUUytWkomkwsnT4qSglxADOU6DoYTtmFkMEI3jMDzVRXnuMuXJhqqNlmRQ3cU6F4Kjxz34S4nRVNhLhDobgqoFZAtOnTQk0NZlXR4IZVM585+M67Iayb0GVEfvYoneoLibNGQSqFD9t940wYavRaBfvnTlQBESriZw6xJnUGLlec38Tmxw1qS5Er3nl/EGKYljrYsEXPCehXI1sIfIlM3WFNsyFmOnYm/G84I6aEss2E7YM6UV5ZTmSxM2vETC7t37ezoelySypVqJpP2XIdlOQ+Z2mHOFnIxs77rdFUzmYzB3C8tnsyPT6ENOESug17aO4kKPKuqQZEBx4ugUuFbmqayyI8VLC2vJBOpeEw2gbqrXUGWPbTRAcaUi/ZLgNTQdLfd5XgOzrayWpqcGLcsC7WctGyQIvADPMdd8MYI3HVb0wAtPM0ZjuP6HguU3LHADmEoynJcjuUMy4RfoQlkt3qBRxFUTOBHkDqXyfXKne5UJsWwdKVcBA4EhvfySpGgyGalkStkatUqRYFJpdebXY5jyXDviyVtw9FoWlheXgaDMJsfM7ROtaK7biBJUq1akRRFV5vxZK7V7CQSMljojmO7llurVVhBAEuNpkigXJ12TVaSQNdMMAR4rtFq02Awej7FApbYer1O0hSwb4BmDbWKcHVVVRS51W7TJAE/ylyoJQ/IB6A4aDnZJKpQRikCF8PW6TuFY64M6T+0tTovoQkDTBYI7+sfqX0ep9+QGgqiCECMnUljxKOdvPOayVFq6KV7Pq816CHoS/6aN8szV4YQuhIVX2C7Dqw0td3kBJHAUJVp07YZmgn5akChnWAUmsYy8MJDpThQxAeiS3CwYdoAICBNQJw6XU0QOFiyaleTY7KuqRzH+z5wI5JhkCca/kQkpt0G5eV6Higc0zRlRbFNE4vyzEM6HIb/ElGPPJbnX4/4pH6tkAvC4whS2w7HtZdqjZl8DgiKUSpJPK3qliTHAhzzbd8N3JgsA6cRZQVUQ7lUBpIEgp4iMUdzMBrjKBboLXDqTDJjW5baaZu25XmYaqiO7omi5nmOTzCWCVyYZmi0/aKpwLrKFM0glw9J6Zqmu4Gjd1HYO1gAJPISwWMTOLbVVvfu2Y1dckgFWFc3dMe+4CaXtuMWkolgBKlNB8uwc2GS5Ozszv4i7E+WFwbg5vJ5lD7lurlcnqLIXr7D+kU+mOy2zTubMjZUdtw0wR4MBpLm4MXEZIBfcpkQRqXolp1UROZCi70WG80g6ikyBCLryuBS/VeDJZrPrmyBnxsnO5cABzaqLb7+lPjr9MCCMPNnbaim4YdJ1Wi7k8BQZjWBwszRKgqVe3+hcQzT77ntjzZkRqPPnjbIV5Sj4Hlt3eRp2gs8zAf7gjANmwbSSNAMBerY8XCCQSWtKXqtrPXwECpqGCYVViFQcsLHr5ayhaHvNdhC7MUEtHOdWudMwXHlirm5yw8pAvlRyJcWTkXe8KsBUXiYBcSyLLm2qAgM62gGKsV2QedD+w1DE9d5mSEVEeRw452/SvDUu3Hk5iB7+MKxhCw5Tq9izQWMMSURGQ7BCFLY2rYoEcZJXiXVeqMN4EEIIC6+Vgzy/MU8FpY2GhbacLkhFU6i57qvVutXlZSCsSudYqM9QRwzGs3n/vpB7EKllJRNHfjp+0dcqrdeA5T476KEf8/Fwr6Orm3jmxWutHSdFSWj0+aHL4b/AgbcbwD2WuhO0tvdI1/9Fnahiyo1N3PDR+4f+aXOkNXIK2N0241KY3zXzuL8KyQrYnhQW15O5HJgK3faBsXQRrflGIYYT5jdphxPZmZ3y294C9BLLaCjF8j5hA2UdmFk0UUh8DhBkZ5lkxznaAbF0RhFe4ZJ85xrWVgQOhPwHjULhkbID4dfKpwXz3HFmOLYdmZqBvWkwwKUCBoEUiIhJDyOF4LA7TYanCjaeoYTFUEWsB+LsUGwsPF4dm6HZXuJ8Yzd6fqe75NMZ7UazxC2lyQJkyJZ4PbdU8uVxdNRLbZI4I/o+cYhZ3qVx7C1GHBUKT8c/UgzTgyzDZKpvt68YmG05bWbzebpgw2cIBtHKceysaioFY635tfQtxbiPIrq3G6Gb5mexK+m9iwggztddcAAxGmO26C74B8Ud06VDshhCowZDkgFwXQqddW1y9b0vshSxvPv/7P/7HvBhawqgCNLjSA1GmdUICgxmqZimXhwYU6EMIyTIEd+qU2lle93SsfBvsZREVhUDRVEF0UTOMthtIiRPI5RAVrJLo4RfuDiesVxA59gWSkb5fL2A10Mo11vLnAcb3W8r339Ecuyw+qXvu+6fshGGJb5td/4VVQsxXXnj59kGdJ1nCefeLxcrTEUg8rnBX4ykfhHH/0oRVGqZhiee/HPDNCTksSzORCKN8eItVrtUYpM+GdUFLQfjYOtvT9wwohY4UPDG4YMUoHfrSxgKPsRp3CMIXEGHq5AY4GMUymY3gBF+HewgMQwG7NavtZwLcwlFYBUT6F4nqHrkizDucrFl8RY9tmnf/Snf/wV17FRIQvLsSzH8zBAFh/jfvGXP82gpmr+yeMLssIenz/++GM/+PvvfAdgLIoSThGT4/m9+/bdeOONzVZbxYJLkKeFYwlROFvBE+F6mJ9/lSIYIOZwtQGOJ5PJbqeLEThcO8qExj3DRMmiqEmTB9yLMgwD/jUzOx11ZRopvi0GQWFh8ypU+SJA2Uieg1G2E9gqhnIhicBYxRkxcAzMQFGenoeOPLPcKdLQOyQVtmBw46rWXCgeTmTwWhELe/yhkpcf+fB9f/lXDw4WZIKfoig6lSrcdNObbcd99NFHQDLt3LnnPfe8p1yunjq1GGCEQ1P068mCARNzc3uxsFJUP4kvLGiORxafjaKlafQp1muYg/faXAVDtfEwZFIqRAU8vyBsIubAdDowezbMMAk4Y3TMNa3aKiPHCBzeIByPdH3UlHFQ6ANoykuv+hhNUPKRF4vtjvUbv3fH//gPz3Bk/vHHfviPP/HB+z78/ge//i1/TUuiZ0NgohBjxkWKwpdXFsNgc0pSpN1ze0WJAzFWr1fE/Jipa13VSKaSvmcHqAAniJsAJB/H0nClrm0ZtiOH3d6ItQrYxeWlRCKhdlFhDF4Q2NDTdvaNwy+uFJcdx9PVLkWzKDcfC2zXZUja89GmgsyLmtGlQKa6vuM4oMRRZDOq/4oWXSKdK4xlR5DaZJlGYirASJhHxCBgPRJeYOO+Z5GWgyr0YpjlipgBsAucsE+MCxqSXBcjK0lKbfmwoXZxkmLw7rGjRdNZve9n7nzLTR+rV6qevlA5NU/RuBWWGQp/FEkFmgFYBLlcbmpyEp6opndBGSUSClAu3w/a7Q6VSBrd5uLplZOvBt12u9uqipLsBYBtwAAHyrlSaUjxZLdRve2dd4/lUZMgzzaPvvzqdftm2x293ZyPZwp79s1hm+WTwQWM5Qu9VoA9prXmfYqaYKGqQPlIHkURVCQZ1WEfurYfw5chgyqcogkE2UMSQViSGVVsxkNZAsoL7QdirhcEFpjcBI3D86cjb2BgW3a9tOz7lucanfo85jLFpdXV063FeXfl5Wf2zbyLY7ml0nyrWP+Nj96/6x3viMoKhJXvgSADOpHeZDnWMi1OFEqrq5quxuLjuq4vLCzEp6ayE9O5yZmo5FV/EZx5nD0MBH2PNslwd737LmA/2bW+klvTdjyqOry8tAR2HxgThm3KAjBIIqyjTwCmk6lciOO4ZeosS2uayrGxmdmJaDN0xKW2ElSINaB6riHTQevP9zwEMS+KgQQxj4w29CnhoJoAAYWjT0O945SWTpim5mh1u6sGYN1hru6YO6f275vL3Hbrmz/3O3/06U+/t9ttMIQwvnuSYYUouDKECKoSRpBktVb5v1/5CroG0J7l0sLJk5OTU3DAq68cvemuu9r16smFJVkG/cU6tgXYBuIM1+OYOo3sCJIRpLndc4MZEORghsJAk9JNTRM4YnxiAj42wvywyDuA5BZJhE3pg3wmEfEDgD5MDLbWhXAkpbZUfL7nLh99Pr9zb4CTUQZdEKASwCEL9bHw6fugGR2MQEoQh7dRy4uwyp3luI888ky7Vp6YVMaTIBOsdqP+lltvvPPdE3/yJ5/ftXvPM08/9/I7rpkoTDXwdvHYUnD09Nve/i4alesNwIQHwkTTzCtHXz704iGagh8FuNiLi4ue5zqOvbyyAk+Xi8Unp3HH0OA3AUM0QceSnNZpE7IkCFyn3VHiiQt+ukunT4MlCxKZokjH8WGBwDqSJBEuwDUdVBqBIjRVj8dTnmcZli0LQqerAtpgzc3MzjCjKnhnb1BEYYlHnnp08eVn5w7ckpqY9nHWD1CzAlRFPgJYCD3XR16Y0HLGAU+E660UK9/93mPPPP080NvZ2dvBCiwurNp8XT/ZHt/9/lZr9d//4e8lJqnf/09/oiTxRsXVdCebTf+rf/e5SJbwqDIsAuxTT/0QB3Ub8hUQVI163TDM1dWirht42HUyiSqVJQcrCWXWWp+ngAyFxsE22o1co+1nL6fC+ISH2uaE/+FnqplHQgg+AkyhFYYTaE7CE2RyuehUQ9Vraci4VIDa29ta6+BDp0VZ5qR4dvfN++/6B83igm0bnushkkVRYbfHSFOgeKPHf/DM93/4vzzbbjc78MQOPnPs/g/em93Z7TZPu475yOPfmZ7InCgvmoYrpeiVUyrNhE2C1ug8PA8hrCuna9rhwy8jresieMF4+fDL7Xb78KGXaIYFRFi2rWnaBctgURDELaoae7axWmv4lkUylONhJKpXhqq6h80sCa3b5QXaMv3sWE5TNTD5WJ5vtTuSyJumBbKaYJjZmR0jSG0+XNcxYNaCoAN/+qWT88fVZvv2n/q40VFffPBvTFWVx/NCKh6bGAdLz9KtJw4e+vL/+x6IGSLs5aLEpFvf/rYb3nRrZSx39Llv4kBvTh9iVjrvUMa/snKS5khRprWOF/Rc0z1pQVEo573Zahq6GXVeDxulY5VapdVsvvzyyxwngpThgeBwr0vFeorhJwrjoGGDoNcdJKrnDlIN9F4wlmdpCkwSWAjJWCy68nQ6FXXVAush6lM/gtTmfinUfMcDggSiHilDMOxefPSbR55+5FN/8MCtP/vpb3/hC9aJxWD+mJCQSz7z1996StccQRKAaoyN52+66YaP/+zHRFGEiXZsN5be+eTB79pYIzBc2SeaJdvvVZ3HBuNiImuLIMjl5ZXQxYhcp+F2m28aeqVSKZdr6dxE5D2KHIz4+h2VqDJMiAFU/D8yBIEXkihNo2fnvWZHeMcyTy6AMHZkWQYRpciSCRAzdMexGEbO5dPVWp1hGRJInucxHKNqOgqH9fxMPq+MvOfbqAdgpmjHBCXgYqDoUGc6IOeW8cDn/vkvfO7zH/ut3/z6X/zvgw/97VxAJ9Oy62IEQyUziQ9//Kc/8JP3cizbf3KCzM0/+erffud7yydP3VfYyRBEUqRqGpzaxVC2JTG4YYdYDkkuLS6CJMBCaz+0BVBljcXTp1yU+4tCtgytu7K0wjKchwcUBpfqiErcMnRQTsDhJYlTuzpgCwxSKkDJm5ws+hbIWiIgyIkdk9vzdobl9u7dd/amp2k7PGqWhEWVF4d/DBek4NEKLI3bdNh7LsBRaGdAoXYqmF5f/LsH/tut936IYWxFYt1MITEz9Z57yemp/O233bjz+lvOlDwI/3zwoT/+zve+ZDuEGMuuuFrME/ZPTf790flw+4UmMAZfhypkr1eqFdRXEm1Xh8IqbLlx/NhJlpPxMKlXEOWZuTkc9TTzfdcOhRDhODLLsqgBCYbFksDq3Uaznc/nkF/DdUgqAe+02uoFZugRRISnK2gMY/M0PHzaBIaTFGp+iBpeoBoq7uEfPMTkd7546Jgys+u9n/hEbmzsJygaC/v1bPDNrJaWb5h791033g8nq3eq/+I3/9X/+eGLvd28MElpfVmEyKoiqrUKFgWbEKFlGVZPP3bi+PjEXF9PmlqXlxSKJKp1NZNNo01cIDQ4prd1XuBpmgGmnEPV0gOaBEOCclyX5/kcv11Msx862y4yRDUYmlTt4YIUKJrDJ04bbdUNvQrwTF3kQ0DVflDNCYo8+h9/54Of+ZW9+28lCbJeq4JRHo+nlPi6EuQwsYWxCYFmHvjL3/3a1//WpIvV2iSBkcilte6oMyLKNE2GZcvlcvQJjkIekJvMcTww2EmKiFrSqp3W0qkFYPw7d++GCygunjJtwzQcRUkQpF9eLWOBnUwXTFNFtahskxVioshx29bwhJs7fOq0jUrgXRSmYH7gh2iavuxuzyFTfBg+nU11OdSrA4OnhBiVi7w54TYeMN9dN71N5MlXX3iy3daKK6uAkUw+d91Nt16//yZRkvCozljoYmh1qun4zFJ5+b67/+HXTj4a4unMVqCPHBC9EEoQEsCKwLDqdtSoY1S4uRjSOd8Hwx9+3QVah2NSLLH3hnhU1TeTz8CPr9uJObPd1n//tf3aBEEAmqnQ5XqRwh05vfBR0tXZU0wGDIPzBGOjnnSAqigwDR4vEJeg1WhKtToiWLg/XkjbtudZ+qEnH/3R97/zxAvHbccmEKEHcYY3ag1XmE9lxUbbDDxuDU9YuC0NT9rrl57zPM+2UGE9kFXoE0RggpBHBbDoWV4IQscj/BPoeaPRQoZDWCUjV8jVqg2CJCxDQzIUJASKoUAOWTD7VMNhadTdZXzHFLmdlEKWAXEJIrHOjBGkBjx+vn/wcDHJ2wyJc2G1G+TpC5kCkA3b82lVW3zlqIM8nIFlO4blKjEFDuNEqdlstRttMBfdwAUDHp4xz3MGwT385DdaJW69pyJ0Tww4IeEp2DaK0cPCtpyhp4AM2yVILMOjkLfwSACYEouzLGMYBtqDCzxJkVE/oHjMCQtdhHoMB0bl2KacoB3L5EV5e7jAb+3MZS5JscaFStUJl8LlRdVwQQqFBnuOYdqpXFLg6HDx4i4KxENo8w13vJAlpJRDoLbpJ+bn09n0+MT4yvJyu9lQG3XMI20fGWOooTRFGyDDUnWuqwRokyQYuOVwx3BggdMM41hu6FgKBjdMOE5AhpvjRllScAACFsPQYUV1GBJyquN6WOEzWFNk6ItRPwVRPIdV5AFkL7j+3SA0geQHUWDxSEqt41I7xurFxWarE0hCNpUAUeUTmOP6yJ2IB916aWWpkc1m9E7HbDefPXniCEMmcwWGZ8bGMsvLjXBygddjFM6SfE1flXyVM+3GgHkHtMzGsahdYg8EYJoZuu77Z7IrI1SxLIeHbRfRthqG2qmbhtqorBI0pbY7ciIZ2Kbj45brxSW+2+nmJnfEY8r5YuESGjcoKHQkpTYQglRmjCE8wrc63e6R4yenJ8ZFnkNGGDAqMAADSu12tUoRR0GPKKhlemehMDtdqam7GGGl2IxKDAKZwXDSN1m3K6Lyr6iKcE8OhrGiQe8RBBE990wTyBQKzepxESSQQHVSDMPiAxEpIJ8yuUyQTaEQnB34muUYRI2s8xcHjqMvPm/hzPTUBJySoWlgeM1mE6PYhMSDsHYxHLCs214ikRQlcXW1BAKu2WyDit81tzseekHxoSiJMHzeczIwBFnCgzhBcr7rVWtNrpCDuQIbEKSEZvvAkGyQ7VqDpXEOZ+DVwuLqW++85/FHnuz1cA2hCX+3G6BVcNP1Bixt3g9MbL29DuZepVKJxZSo0lW4XwwqxOMYgUDRfYHj2BEHW+vgQkSZUv28lSgZYRvZ0ABwoJ1EKrZ1gZCXX3i+arqd1jUvvXRofCxfbTazmQzc9fLiyXwqC5J37+7p06VmcfHE3P7bGuUVMEGRExjzvvfID375V35NYEfBLVtYLRzukoFrcVlRKTi2XlwuTY4jke6iQDmQGjGawHzG5kmn1ep4AfPMs8c/8qlfffWFFx793rMBcCyfDC3EQFTiYNZ1mjVP6w5wNdr3tFD3rWMhKysrMUVBORSB3yPwAQ5ECiw+P4zRi/pRG6bRbHdQPA1GKJKk6abWbceTqVQivv19tdrtXbOzK8ViXNlSLd7/0U+omi5L4s1vOuChzWMC1DcD1i98vdmgaEYUxf0kehMPHR+RJWEY+nsZTuTo0R7floOkCMew0+PTQn7OrL6ixCTX81HNexZzPFKKxQOHkGg9rkw0nnhSazvvuu+DLx58WpESosB3DZQAg1gXw/yz3/7XoA4e+973H/if/7W7MB+dO4rxDbl5302FuFS5XN53zb4oPIFAdARBi2E4EASI6yM/J0IVx3E51AfWD61+1BI3k0qcy6OEpbJaLmNrNbE3LY4N5/yrv/piTBIAIdfuu/7JJ76P0yzYlbuuueGFg497GFHIJkAgNhrNHZPTpaXTSiazsnT6xptvEZTk9Xt3jjZkttN9MOFq+YShdhF/Ioh2V8+n4wQqlEf5tmZ1u4HA1Q11+rqbfvKGW1588emJ/PjySjGVS5jLDcL0Pc8hXOLP/ssfgsqyNLXZrPWfGhYS2BBP3uDzbrWbfV2IeFSAdvoYlg37CgdhSG60UxPVvSLXQHCulv/Mjh3r9pu2ME3ee/c9iUTCdlxJkpTYvRzPg9JOJuO7pgqyKDSadbRHRWC6Yd5wYD8Yxw+WVmZ2ziVi0lA9wKGDlCgJrmWqagOMKwxFMyJmSjEUEaD8YsJVu43qsaPtW6+b3H/7XQcf+76kJF2Me/xQ6Y673nnfR645duT5R7/zTWAthRh3+OhCrV4DJsSyLBI/OBduqzBR1ZRIp8ADhuf32X/722D2P/7449hapyQUPUxSOI6t1T/EBUk8/8bqvb4hKME8itdE/UmIrUTZzOxs/1+CMNV/nUqikPN8vrDhG//05z+FDd8Ylk5X+2enidBJfbLe7GXl9RKzsbAoasR/e25JAkHNj3ocBmsbppFSQ88vyhjx/TOuZHzNx4mdsYng7x2JOIV8BH61Wo3yGoKt0yxjiUSvYOv5+kV6HlAiuh6KRADb0OmKHWgRcMEzOep0tYVzJaxZsMWa7/3ZP8D3/A3m21rGy7nwm96Ldrt98803G4YReXT6dGewQQiItC99++FUOnPR9gc2l8v+eFeoGVVuwQZzpAa58zpsXSLn4flaZbA8Op2Opmm+7wGs4/EEs9bkYwSp8xiNcvHloycKhVy71cRci2DFRq3BCXw6GWs02gTu75jdefzYMTCHWLAEMfy6A28iideY5Va1tLTayMSYer2VzGRxihsrZAeVb19fDCq+/sMLP8J7DW3XHxalr0SFLqK0O6Tl1gITUJ909GYQ6T3ynFUSfBfA1Gw24YStalHtdsZ27FRVFThfLpunKGoEqfMYDC/Ozc24np/Nj8myaJr2julpx3Vtw0jl8iePvsLywvTOOZhcxzIphn1NPMGQ4smME/AsNRnL6HpXickbnt8GCrJhWyP6qFZcsT185fTJarV24803Hzl0iOUlmsSMboeWMwmJqFdrHiXc+c47Ispmae0jR08nJLy4WlENa9fea3btnA7OoSpGH0+gxQ9+62/UV5544ZWT3YD99d//73gqUywuj49PDlWi1bBDCtWQYPlsaOaAylHiQXm1lMlmgDhRFHnL7W+Hd+OJxPndJ83kw55sMJR4bPDhRe4AECcRdCL09AlZH22AESWZbLW6u6+5dsZ20tlsIpU2NNULgz9RYz8Cy0/O4IjC97DIirH8WFJRJDmZAaOBF8+1trbjOMDw4DxwfnfpxZ/5yff9xA3HH3rs+W/8zV987Jd+03W9ZrORSqWHUwMOI6R811ldLr3y/DOu45mOk0wmQFAdftFWYR7HJt76jndc2hKMEaqcsLUwy7IRWYny+PriJPpFimY9p0aIvMjxRw4fnpye5kQJFVDzXLXV9FiBQv0gCcuyuDBgHM5TmECpNZJyfnRKVfUA+Vx9kmZi6UKx2tZ0n2Q4JZYKCy4E3a4KvGo41d8wXlMslb0ulcXx/cHArmwUhBlt0F5yeg5IilgL4CkKTIiyjUNHOcnzfGF8nGboRmX18PPPEwx/4NbbbNs9OX+y1WxwDKGrmhKP16v1ZC7TqlT2v/Ud3DY5CK+l+OBT23HC2yR4zLtmcqzb7Rw/voQTdLa2HGUho2NsewSpcx2WbZtRd+HXzRsmCALD0GsSiLzmmmtA8ZXL5SinL0zrIxKJxPT0dKFQgBc8Op6VC5N33z8VASKTelOw3uF16a4OW6sOifmclN51fbL0aud0jOw0d7zvI32zdJtM+RGkNg42HG/Yz9E0fdddd+VyuePHj4+PjyeTSQCcJEkTExPwGgDUaDTgI9dxLMep1esXjBSAZjIePxffVVjM1UN7kQTVlcZiwlJCEI7V6vtn9/Ql2YieD7VfaufOnTMzMxgKwxRBXOFrRXZAG87Pzx89evTEiRPv++mfSSRTyuvfvgbVT6MpIGTw2rA8iZJVGy95tuMSjWYrhbY6kEHADFOt82GHFDBly7QEFGUbRXIEqH0dFqBMS5LqlSsI1Q/y+oQ7u0TYPkw3TcKzGU7qb9REbcpME/gy1yNiZ+lT+HqpVFpcXARtK8uyrusgt0AVwutDhw4dPHiwWCyiWoYDBGgDGRos8tR7sXbEhTE/UeBfnC997QfHv/ujUz//wTffPPs2mz1J3HLXr/7On01MzfzaJ+6em50YWtfU0F0WPAmt21E7nUqlorYbFEnLimxbdqVen53eoesmRZG8wNdKVSEeCzyXY6lSqUJTvGNZvMKF+8scSQQ+gRgsw/KdVpPkOMp3bcffu3//2YG5QEpADoGgmpycBPL7xS9+cffu3QDBJ5544qtf/SoIKsTQOR5w3G03Hc/nOMnWuwB7lhNUvcNQKJhY73SVRIymqEajLcoCXAxBEag+XwA/TnuokJ+TSCbOEV8sS7+6pD74/aNwrX/whUc9x8Ztx9Af8Xz8RL04v/Lgo1/8rZGr85zFPtqgTcL/wnU+jRIcwt2R2fUxsKlsrv86W5jagiKj3KnC1NQ66nvWAFHX7XaB6ICU0jTt9ttvB/n0wAMPgIgyDAMkVjqd/uxnPxuPx8HICiyr0yi7rgewBrRRDGgo2/O9WDzWbqOT+IEL66HVaObzWTdAsrVebfGisLxcTCTPw5f2Cx+4kWep3/7833ue7XumH9ABzRM+dt3eya/80adiQ9ySaSgtPkPvdlVUXMNxW+2uIjKiJFiGHe50+CyvuLaFhXsjHM90Vd13LaCxHM/bpjU+NTXgBMfPpVkwSKmHHnoIcHPgwIFsNguC6rnnngM+DiYC4Onaa6/9zGc+A1oYNCnLczTDKPF4EPiAqlQ2P4hLOR6HC5RjKFVmfGoHNtA2KPB94GHnpQJhLX3i3uvf85aZ7z514uEnDqmaef3c2H13Xr9/70S/6/oIUuc8m2Erac91BUWBp0gSBI1qjnOhBzJgWI6URNdzLMOCxy/JsiDk1G6HYRmOFwYq8p7HAPEDgur6668HHQc8/ZZbbgHyBCzq/vvvv+OOO+B3Ty8u7suNGZq2enqB5KTC5FS9UgWTy4RvGibLsLZt4qj6v8eLMd93ABGeqeUKhWq56qCw+WBq5yyJnR8UUJnhlPyxew989H37sYtgZiNIobI48L8eURU3l/AMxgrimWhG9uIqiQEwvvzlL7/3ve+dnp6OfJuf+tSn9u/fPzs7u7CwAEQYADdtGIIkFmbmUMo5Q2fzOc+LOHu4XYNqi6FsQcB+GDvaS/rLjk+Ekew+TV74VF8RSDojEUZOhGjU6/XPf/7zQJ7Aeq9Wq0eOHAFq9e1vf/vEiRNgD37zoYcAakC6Uf1gx261EF1yHNe2QQmb6Fvoi6Zj27rWBQyZpuGhMvcoyJgggKHzVxYsLkpKiQxtddqXz8BDyxyICRjq0d6LqelvTKoH/Nyqi/Y9ABAf//jHQdMxDNNut+EawNgEMt5qtfbt2wem36uvvvqmG290dY0kiU6jBSan6/qg9bq1hiDxII4M3QAxCUTPUHVeEkCJOpYOGth2bI5hWBGNMz/qOVHsi6OpUU5OaXWVvBRxmJba7Ud4Xa7nyVPU/xdgACC4A0or4DX9AAAAAElFTkSuQmCC' />
                  <FormInfoList>
                    <FormInfoListItem>Kostenlos und unverbindlich</FormInfoListItem>
                    <FormInfoListItem>Individuell anpassbar</FormInfoListItem>
                    <FormInfoListItem>Alle Leistungen und Preise detailliert aufgelistet</FormInfoListItem>
                  </FormInfoList>
                </FormInfo>
              </FormContent>
            </FormContainer>
            <FormFooter>
              Ihre Daten werden sicher übertragen und für Ihre Beratung genützt.
              Sie erklären sich damit einverstanden, dass Benu Sie zu
              Beratungszwecken telefonisch und per E-Mail kontaktieren darf.
            </FormFooter>
          </FormElement>
          <Submit type='submit' disabled={!this.formValid(this.state.errors)}>
            ZUM ANGEBOT
          </Submit>
        </form>
      </div>
    )
  }
}
