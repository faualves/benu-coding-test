export default{
    // Backgrounds
    backgroundBody: '#d5e4e6',
    backgroundContent: '#fff',
    backgroundHeaderBox: '#a7c9cd', 
    backgroundBottomBar: '#cacaca',

    // Buttons
    redButton: '#974353',
    redButtonDisable: '#c6a3a9',

    // Texts
    primaryText: '#4a4a4a',
    placeholderText: '#9b9b9b',

    // Elements
    checkIcon: '#91dd98',

    // Form
    elementBorder: '#a7c9cd'

}
