import React from 'react';
import styled, { createGlobalStyle }  from 'styled-components'
import Colors from './Colors'
import MaskedInput from 'react-text-mask'

export const GlobalStyles = createGlobalStyle`
  body {
    font-size: 10px;
    background-color: ${Colors.backgroundBody};
    height: 100%;
  }
`
export const PageContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
  padding-top: 60px;
`;
export const FormElement = styled.div`
  width: 496px;
  background-color: ${Colors.backgroundContent};
  font-size: 10px;
  box-shadow: rgba(0, 0, 0, 0.40) 10px 10px 20px;
  margin-top: 20px;
`;
const FormTitleArea = styled.div`
  position: relative;
  background-color: ${Colors.backgroundHeaderBox};
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  top: -20px;
  width: 180px;
  height: 38px;
  margin: 0 auto;
`;

const FormTitleText = styled.p`
  color: ${Colors.primaryText};
  font-size: 14px;
  font-weight: 700;
`;

export const FormTitle = props =>{
  return(
    <FormTitleArea>
      <FormTitleText>{props.children}</FormTitleText>
    </FormTitleArea>
  );
}
export const FormContainer = styled.div`
  margin: 0px 27px;
`;

export const FormContent = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-start;
  padding-top: 5px;
`;

export const FormArea = styled.div`
  width: 220px;
  display: flex;
  flex-direction: column;

`;

export const FormRadioArea = styled.div`
  display: flex;
  height: 29px;
  width: 100%;
`;

export const FormRadioItem = props => {
  let {value ,name, checked, onChange } = props;
  return(
    <RadioLabel>
      <RadioCheckMark>
        {checked && (<RadioCheckMarkChecked />)}        
      </RadioCheckMark>
      <RadioButton 
        type='radio'
        name={name}
        value ={value}
        checked ={checked}
        onChange={onChange}
      />
      {props.children}
    </RadioLabel>
  )
}

const RadioLabel = styled.label`
  display: inline-block;
  position: relative;
  padding-left: 21px;
  margin-bottom: 12px;
  margin-right: 20px;
  cursor: pointer;
  color: ${Colors.primaryText};
  font-size: 13px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
`;

const RadioButton = styled.input`
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
`;

const RadioCheckMark = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 12px;
  width: 12px;
  border-style: solid;
  border-width: 2px;
  border-color: ${Colors.elementBorder};
  border-radius: 50%;
`;

const RadioCheckMarkChecked = styled.div`
  margin: 2px;
  height: 8px;
  width: 8px;
  background-color: ${Colors.elementBorder};
  border-radius: 50%;
`;

export const FormInput = props => {
  return (
  <FormInputArea>
    {props.name === 'phone' 
      ?(    
        <FormInputPhoneField 
        type={props.type}
        placeholder={props.placeholder}
        name={props.name}
        onChange={props.onChange}
        error={props.error}
        mask={['(', /\d/, /\d/,/\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
        />
      )      
      :(
        <FormInputTextField 
        type={props.type}
        placeholder={props.placeholder}
        name={props.name}
        onChange={props.onChange}
        error={props.error}
        />
      )
  }
    {props.error.length > 0 && (
        <FormInputTextErrorArea>
          <FormInputTextErrorText>{props.error}</FormInputTextErrorText>
        </FormInputTextErrorArea>
      )
    }
        
  </FormInputArea>
)}

const FormInputArea = styled.div`
  height: 53px;
  width: 100%;
`;

const FormInputTextField = styled.input`
  -webkit-appearance: none;
  font-size: 15px;
  color: ${Colors.primaryText};
  border: 0px;
  ${props => props.error ? `border-bottom-color: ${Colors.redButton}` : `border-bottom-color: ${Colors.elementBorder}`}
  border-bottom-style: solid;
  border-bottom-width: 2px;
  font-weight: 300;
  height: 30px;
  padding: 3px 8px;
  width: calc(100% - 16px);
  outline: none;
  ::placeholder,
  ::-webkit-input-placeholder {
    color: ${Colors.placeholderText};
  }
  :-ms-input-placeholder {
    color: ${Colors.placeholderText};
  }
`;
export const FormRequiredItem = styled.p`
  width: 100%;
  text-align: center;
  font-size: 13px;
  font-weight: 300;
  line-height: 17px;
  margin: 2px;
  color: ${Colors.primaryText};
`;
const FormInputPhoneField = styled(MaskedInput)`
  -webkit-appearance: none;
  font-size: 15px;
  color: ${Colors.primaryText};
  border: 0px;
  ${props => props.error ? `border-bottom-color: ${Colors.redButton}` : `border-bottom-color: ${Colors.elementBorder}`}
  border-bottom-style: solid;
  border-bottom-width: 2px;
  font-weight: 300;
  height: 30px;
  padding: 3px 8px;
  width: calc(100% - 16px);
  outline: none;
  ::placeholder,
  ::-webkit-input-placeholder {
    color: ${Colors.placeholderText};
  }
  :-ms-input-placeholder {
    color: ${Colors.placeholderText};
  }
`;


const FormInputTextErrorArea = styled.div``;
const FormInputTextErrorText = styled.p`
  margin: 0;
  `;

export const FormInfo = styled.div`
  display: flex;
  width: 198px;
  flex-direction: column;
`;

export const FormInfoTitle = styled.p`
  margin: 0px;
  font-size: 16px;
  font-weight: 700;
  color: ${Colors.primaryText};
  margin-bottom: 14px;
`;

export const FormInfoImage = styled.img`
  width: 198px;
  height: 146px;
`;

export const FormInfoList = styled.ul`
  margin: 8px 0px 0px 14px;
  padding-left: 5px;

`;

export const FormInfoListItem = styled.li`
  margin: 0px;
  padding: 0px;
  font-size: 13px;
  font-weight: 300;
  line-height: 17px;
  color: ${Colors.primaryText};
  margin-bottom: 4px;
`;

const FormCheckIconArea = styled.div`
  justify-content: center;
  align-items: center;
`;
const FormCheckIconSVG = styled.img`
  width: 40px;
  height: 40px;
  margin-left: auto;
  margin-right: auto;
  display: block;
`;
export const FormCheckIcon = props => (
  <FormCheckIconArea>
    <FormCheckIconSVG src='data:image/svg+xml;base64,CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJub25lIiB4PSIwcHgiIHk9IjBweCIgd2lkdGg9IjQwcHgiIGhlaWdodD0iNDBweCIgdmlld0JveD0iMCAwIDQwIDQwIj4KPGRlZnM+CjxnIGlkPSJMYXllcjFfMF9GSUxMIj4KPHBhdGggZmlsbD0iIzkxREQ5OCIgc3Ryb2tlPSJub25lIiBkPSIKTSA0MCAyMApRIDQwIDExLjcgMzQuMTUgNS44NSAyOC4zIDAgMjAgMCAxMS43IDAgNS44NSA1Ljg1IDAgMTEuNyAwIDIwIDAgMjguMyA1Ljg1IDM0LjE1IDExLjcgNDAgMjAgNDAgMjguMyA0MCAzNC4xNSAzNC4xNSA0MCAyOC4zIDQwIDIwCk0gMjcuMzUgMTEuNTUKTCAzMC4zNSAxNC4yIDE2LjggMjkuNSAxMCAyMi43IDEyLjg1IDE5Ljg1IDE2LjY1IDIzLjY1IDI3LjM1IDExLjU1IFoiLz4KPC9nPgo8L2RlZnM+Cgo8ZyB0cmFuc2Zvcm09Im1hdHJpeCggMSwgMCwgMCwgMSwgMCwwKSAiPgo8dXNlIHhsaW5rOmhyZWY9IiNMYXllcjFfMF9GSUxMIi8+CjwvZz4KPC9zdmc+Cg==' />
  </FormCheckIconArea>
);
export const FormContentTitle = styled.p`
  justify-content: center;
  align-items: center;
  color: ${Colors.primaryText};
  font-size: 22px;
  font-weight: 700;
  text-align: center;
  line-height: 34px;
  margin: 0px;
  margin-top: 8px; 
  
`;
export const FormContentSubtitle = styled.p`
  justify-content: center;
  align-items: center;
  color: ${Colors.primaryText};
  font-size: 13px;
  font-weight: 300;
  text-align: center;
  line-height: 20px;
  margin: 0px;
  margin-top: 15px;  

  margin-bottom: 10px; 
`;

const FormFooterArea = styled.div`
  display: flex;
  background-color: ${Colors.backgroundBottomBar};
  padding: 10px 15px;
  margin-top: 15px;  
`;

const FormFooterLock = styled.img`
  width: 28px;
  height: 36px;
`;

const FormFooterInfo = styled.p`
  color: ${Colors.primaryText};
  font-size: 11px;
  font-weight: 300;
  padding: 0px 15px;
  margin: 0px;
`;

export const FormFooter = props => (
  <FormFooterArea>
    <FormFooterLock src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAkCAYAAACaJFpUAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyVpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQ4IDc5LjE2NDAzNiwgMjAxOS8wOC8xMy0wMTowNjo1NyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIDIxLjAgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RDJCQkMyNzk0QzRCMTFFQTkwN0Q4Q0QzQ0U4QjI4NUQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RDJCQkMyN0E0QzRCMTFFQTkwN0Q4Q0QzQ0U4QjI4NUQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpEMkJCQzI3NzRDNEIxMUVBOTA3RDhDRDNDRThCMjg1RCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpEMkJCQzI3ODRDNEIxMUVBOTA3RDhDRDNDRThCMjg1RCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PjyF604AAAI7SURBVHjaYvz//z8DIXDw0OH/N28/Yrh28wHD4yevGL58/c7AzsbKICzEx6CtocSgoSbH4OPtzshABGAkZOHK1Rv/L16xg+H7j5941dlZGTC4OpoyWFlZMpJtYWvntP8Hjpxl+PcPoUZJQZpBXEyI4evXbww3bz9m+PnrF1wOJL5sfg95Fi5fuf7/vCVbwJaBgs/Xy4ZBW12Rwc7OBm7giRMn/z97/pph47bDDE+evQaLyUqLM2QkBTBYWJhjtZgFm+D+A4f+L121C2wZJyc7Q1KMN0NQgA+GATBDJSWE/y8Bqr9x6yHD46cvGW7fewySw+oRJmyCd+49gceZj7sVVsuQgaWlJWOgtx0DOzsbmL9r32mcarFaCHIpDGioyhOT+BhcXBwZLU11wGxQMO/Yuec/0Ra+//AFTPPz8zA4ONgRldxBQFlRCs4GZR2iLfz5E5Ly+Hi4GEgBbMDEBQO/f/8m3kJ4EmZkZKA2YGKgMxi1cNRCkgHjlaOT/ot/zUMRBFUOoDIdlCuYSMwZf/9BaGYsXnnJPQl74Q22hMwsyMw0mmjoDFhI1bD3sjLDimNmDKC6J8T8PIOHwQ3a+nDDGSOGT985GD4D8bpThvBUSTML//5DaPn3j5GBiFbmaColUDQx/EduZNLeQn35Z0DyPxjryz0nWLJQnC3SXI4wWKidAiYeNgZDhS+0z4cgoCcHamT9Gi3asAfptSfcDO2bHOlimb8fNwPL91/MDHdf8dPFQpBdwz8OAQIMAAWXxBZT0pJgAAAAAElFTkSuQmCC' />
    <FormFooterInfo>{props.children}</FormFooterInfo>
  </FormFooterArea>
);

export const Submit = props => {
  let { disabled } = props
  return(
    <SubmitButtonArea>
      {disabled
        ? (<SubmitButton type='submit' disabled>{props.children}</SubmitButton>) 
        : (<SubmitButton type='submit'>{props.children}</SubmitButton>)}
      
    </SubmitButtonArea>
  )
}

const SubmitButtonArea = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 100px;
`;

const SubmitButton = styled.button`
  ${props => props.disabled ? `background-color: ${Colors.redButtonDisable}` : `background-color: ${Colors.redButton}` }
  width: 196px;
  height: 55px;
  color: #fff;
  border-width: 0px;
  font-size: 17px;
  font-weight: 700;
  box-shadow: rgba(0, 0, 0, 0.40) 10px 10px 20px;
  letter-spacing: 3px;
  outline: none;
`;